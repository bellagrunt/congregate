pragma solidity ^0.6.0;

contract Congregate {
  struct Location {
    int latitude;
    int longitude;
  }

  Location public location;
  string public name;
  bytes32 public description;

  mapping(address => bool) public owners;
  mapping(address => bool) public members;
  mapping(bytes32 => bool) public tags;

  modifier isOwner() {
    require(owners[msg.sender] == true, "Must be an owner to authorize this action");
    _;
  }

  event LocationUpdated(int _latitude, int _longitude);
  event NameUpdated(string indexed _name);
  event OwnerAdded(address _owner);
  event OwnerRemoved(address _owner);
  event MemberJoined(address _member);
  event MemberLeft(address _member);

  constructor(string memory _name, bytes32 _description, address[] memory otherOwners) public {
    name = _name;
    description = _description;
    // Default location to null island
    location = Location({
      latitude: 0,
      longitude: 0
    });

    // Make the person deploying the contract the first owner
    owners[msg.sender] = true;
    // Add all other owners
    for(uint i = 0; i < otherOwners.length; i++) {
      owners[otherOwners[i]] = true;
    }
  }

  //
  // Owner methods
  //

  function addOwner(address _owner) public isOwner {
    owners[_owner] = true;
    emit OwnerAdded(_owner);
  }

  function removeOwner(address _owner) public isOwner {
    owners[_owner] = false;
    emit OwnerRemoved(_owner);
  }

  // Set the location and return it back as a tuple
  function setLocation(int _latitude, int _longitude) public isOwner returns (int, int) {
    location.latitude = _latitude;
    location.longitude = _longitude;

    emit LocationUpdated(location.latitude, location.longitude);
    return (location.latitude, location.longitude);
  }

  function addTag(bytes32 _tag) public isOwner {
    tags[_tag] = true;
  }

  function removeTag(bytes32 _tag) public isOwner {
    tags[_tag] = false;
  }

  function setName(string memory _name) public isOwner {
    name = _name;
    emit NameUpdated(name);
  }

  function setDescription(bytes32 _description) public isOwner {
    description = _description;
  }

  //
  // Public methods
  //

  function join() public {
    require(members[msg.sender] != true, "This address is already a member of the congregation");

    members[msg.sender] = true;
    emit MemberJoined(msg.sender);
  }

  function leave() public {
    require(members[msg.sender] == true, "Must be a member of a congregation to leave one");

    members[msg.sender] = false;
    emit MemberLeft(msg.sender);
  }
}