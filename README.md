<div align="center">
  <img src="https://i.imgur.com/851NWvq.png" alt="Congregate" />
</div>

A blockchain based meetup app, where each participant owns a share of the community. This repo consists of ethereum based contracts for DAO, Badge & Directory logic, as well as a frontend application to interact with the blockchain.

### Get Started

`npm i` -- install
